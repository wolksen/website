<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->


    <head>
        <!-- BASICS -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Wolksen</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="css/isotope.css" media="screen" /> 
        <link rel="stylesheet" href="js/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/bootstrap-theme.css">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/languages.min.css">
        <!-- skin -->
        <link rel="stylesheet" href="skin/default.css">
    </head>
     
    <body>
        <section id="header" class="appear"></section>
        <div class="navbar navbar-fixed-top" role="navigation" data-0="line-height:100px; height:100px; background-color:rgba(0,0,0,0.3);" data-300="line-height:60px; height:60px; background-color:rgba(0,0,0,1);">
             <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="fa fa-bars color-white"></span>
                    </button>
                    <h1>
                        <a class="navbar-brand" href="/index.php">
                            <img src="img/logo.png" title="Wolksen" alt="Wolksen" data-0="margin-top: -10px;" data-300="margin-top: -35px;" />
                        </a>
                    </h1>
                </div>
                <div class="navbar-collapse collapse" data-0="padding-top: 15px;">
                    <ul class="nav navbar-nav" data-0="margin-top:20px;" data-300="margin-top:5px;">
                        <li class="active"><a href="/index.php">Home</a></li>
                        <li><a href="#section-works">Solutions &amp; Aplications</a></li>
                        <!--<li><a href="#section-about">Team</a></li>-->
                        <li><a href="#section-contact">Contact</a></li>
                        <li><a href="index-ptbr.php"><span class="lang-lbl" lang="pt"></span></a></li>
                    </ul>
                </div><!--/.navbar-collapse -->
            </div>
        </div>

        <section class="featured">
            <div class="container"> 
                <div class="row mar-bot40">
                    <div class="col-md-6 col-md-offset-3">
                        
                        <div class="align-center">
                            <img src="img/nuvem.png" />
                            <h2 class="slogan">Welcome to Wolksen</h2>
                            <p>

                            We believe that eficiency makes the world better. Using web-based sensors to collect and analyze data, it's possible to create smart machines, which not only perceive and interact with its enviroment, but also communicate with people and other machines.
                
                            </p>    
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <!-- services -->
        <section id="section-services" class="section pad-bot30 bg-white">
        <div class="container"> 
        
            <div class="row mar-bot40">
                <div class="col-lg-4" >
                    <div class="align-center">
                        <i class="fa fa-globe fa-5x mar-bot20"></i>
                        <h4 class="text-bold">Dashboards</h4>
                        <p>We provide a SAAS platform to analyse data from your business.
                        The data is provided in realtime from our IoT devices.
                        </p>
                    </div>
                </div>
                    
                <div class="col-lg-4" >
                    <div class="align-center">
                        <i class="fa fa-wifi fa-5x mar-bot20"></i>
                        <h4 class="text-bold">Connectivity</h4>
                        <p>We provide the solution for your demand, even on places with a poor internet connection. Our devices run with GPRS, WiFi, Bluetooth or RFID.
                        </p>
                    </div>
                </div>
            
                <div class="col-lg-4" >
                    <div class="align-center">
                        <!-- <i class="fa fa-stethoscope fa-5x mar-bot20"></i> -->
                        <img src="img/icon/electrical_sensor.png" height="100" alt="" />
                        <h4 class="text-bold">IoT Sensors</h4>
                        <p>The key of good analysis is a realible data. Our IoT sensors work under utmost precision. When team up with our cloud platform it becomes a powerful asset for your company.
                        </p>
                    </div>
                </div>
            
            </div>  

        </div>
        </section>
            
        <!-- spacer section:testimonial -->
        <section id="testimonials" class="section" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row">               
                    <div class="col-lg-12">
                            <div class="align-center">
                                        <div class="testimonial pad-top40 pad-bot40 clearfix">
                                            <h5>
                                                 “One of the myths about the Internet of Things is that companies have all the data they need, but their real challenge is making sense of it. In reality, the cost of collecting some kinds of data remains too high, the quality of the data isn’t always good enough, and it remains difficult to integrate multiple data sources.” — 
                                            </h5>
                                            <br/>
                                            <span class="author">&mdash; Chris Murphy, Editor from Information Week</span>
                                        </div>

                                </div>
                            </div>
                    </div>
                
            </div>  
        </div>  
        </section>
            
        <!-- section works -->
        <section id="section-works" class="section appear clearfix">
            <div class="container">
                
                <div class="row mar-bot40">
                    <div class="col-md-offset-3 col-md-6">
                        <div class="section-header">
                            <h2 class="section-heading animated" data-animation="bounceInUp">Solutions &amp; Aplications</h2>
                            <p>A broad range of industries can benefit from IoT applications. Here are some of our lastest works. </p>
                        </div>
                    </div>
                </div>
                    
                        <div class="row">
                          <nav id="filter" class="col-md-12 text-center">
                            <ul>
                              <li><a href="#" class="current btn-theme btn-small" data-filter="*">All</a></li>
                              <li><a href="#"  class="btn-theme btn-small" data-filter=".aplications" >Industries</a></li>
                              <li><a href="#"  class="btn-theme btn-small" data-filter=".solutions">Products</a></li>
                            </ul>
                          </nav>
                          <div class="col-md-12">
                            <div class="row">
                              <div class="portfolio-items isotopeWrapper clearfix" id="3">
                              
                                <article class="col-md-4 isotopeItem aplications">
                                    <div class="portfolio-item">
                                        <img src="img/portfolio/oil.jpg" alt="" />
                                         <div class="portfolio-desc align-center">
                                            <div class="folio-info">
                                                <h5><a href="#">Oil &amp; Gas Distribution</a></h5>
                                                <a href="img/portfolio/oil.jpg" class="fancybox"><i class="fa fa-plus fa-2x"></i></a>
                                             </div>                                        
                                         </div>
                                    </div>
                                </article>
<!--
                                <article class="col-md-4 isotopeItem solutions">
                                    <div class="portfolio-item">
                                        <img src="img/portfolio/rs232.jpg" alt="" />
                                         <div class="portfolio-desc align-center">
                                            <div class="folio-info">
                                                <h5><a href="#">I2C to RS232 Conversor</a></h5>
                                                <a href="img/portfolio/rs232.jpg" class="fancybox"><i class="fa fa-plus fa-2x"></i></a>
                                             </div>                                        
                                         </div>
                                    </div>
                                </article>
-->
                                <article class="col-md-4 isotopeItem aplications">
                                    <div class="portfolio-item">
                                        <img src="img/portfolio/factory.jpg" alt="" />
                                         <div class="portfolio-desc align-center">
                                            <div class="folio-info">
                                                <h5><a href="#">Manufacture Quality Control</a></h5>
                                                <a href="img/portfolio/factory.jpg" class="fancybox"><i class="fa fa-plus fa-2x"></i></a>
                                             </div>
                                         </div>
                                    </div>
                                </article>

                                <article class="col-md-4 isotopeItem aplications">
                                    <div class="portfolio-item">
                                        <img src="img/portfolio/cold_storage.jpg" alt="" />
                                         <div class="portfolio-desc align-center">
                                            <div class="folio-info">
                                                <h5><a href="#">Cold Storage Monitoring</a></h5>
                                                <a href="img/portfolio/cold_storage.jpg" class="fancybox"><i class="fa fa-plus fa-2x"></i></a>
                                             </div>
                                         </div>
                                    </div>
                                </article>

                                <article class="col-md-4 isotopeItem aplications">
                                    <div class="portfolio-item">
                                        <img src="img/portfolio/water.jpg" alt="" />
                                         <div class="portfolio-desc align-center">
                                            <div class="folio-info">
                                                <h5><a href="#">Water Supply Submetering</a></h5>
                                                <a href="img/portfolio/water.jpg" class="fancybox"><i class="fa fa-plus fa-2x"></i></a>
                                             </div>                                        
                                         </div>
                                    </div>
                                </article>
                                
                                <article class="col-md-4 isotopeItem solutions">
                                    <div class="portfolio-item">
                                        <img src="img/portfolio/dashboard.jpg" alt="" />
                                         <div class="portfolio-desc align-center">
                                            <div class="folio-info">
                                                <h5><a href="#">Custom Web Dashboards</a></h5>
                                                <a href="img/portfolio/dashboard.jpg" class="fancybox"><i class="fa fa-plus fa-2x"></i></a>
                                             </div>                                        
                                         </div>
                                    </div>
                                </article>

                                <article class="col-md-4 isotopeItem aplications">
                                    <div class="portfolio-item">
                                        <img src="img/portfolio/harvest.jpg" alt="" />
                                         <div class="portfolio-desc align-center">
                                            <div class="folio-info">
                                                <h5><a href="#">Agriculture &amp; Livestock<br>Supply Chain</a></h5>
                                                <a href="img/portfolio/harvest.jpg" class="fancybox"><i class="fa fa-plus fa-2x"></i></a>
                                             </div>                                        
                                         </div>
                                    </div>
                                </article>
                                
                                </div>
                                        
                            </div>
                                     

                            </div>
                        </div>
                
            </div>
        </section>
        <!-- section works -->
        
        <!-- spacer section:stats -->
        <section id="parallax1" class="section pad-top40 pad-bot40" data-stellar-background-ratio="0.5">
            <div class="container">
            <div class="align-center pad-top40 pad-bot40">
                <blockquote class="bigquote color-white">It's all about delivering value to your business.</blockquote>
                <p class="color-white">If you have something or other application in mind: Let us know.</p>
            </div>
            </div>  
        </section>
        

        <!-- about -->
        <!--
        <section id="section-about" class="section appear clearfix">
        <div class="container">

                <div class="row mar-bot40">
                    <div class="col-md-offset-3 col-md-6">
                        <div class="section-header">
                            <h2 class="section-heading animated" data-animation="bounceInUp">Our Team</h2>
                            <p>Meet the awesome people behind this project. </p>
                        </div>
                    </div>
                </div>

                    <div class="row align-center mar-bot40">
                        <div class="col-md-3">
                            <div class="team-member">
                                <figure class="member-photo"><img src="img/team/afonso.jpg" alt="" /></figure>
                                <div class="team-detail">
                                    <h4>Afonso Coutinho</h4>
                                    <span>Hardware Engineer</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="team-member">
                                <figure class="member-photo"><img src="img/team/ismael.jpg" alt="" /></figure>
                                <div class="team-detail">
                                    <h4>Ismael Rodrigues</h4>
                                    <span>Business Developer</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="team-member">
                                <figure class="member-photo"><img src="img/team/vido.jpg" alt="" /></figure>
                                <div class="team-detail">
                                <h4>Lucas Vido</h4>
                                    <span>Software Engineer</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="team-member">
                                <figure class="member-photo"><img src="img/team/romulo.jpg" alt="" /></figure>
                                <div class="team-detail">
                                <h4>Rômulo Nascimento</h4>
                                    <span>Product Manager</span>
                                </div>
                            </div>
                        </div>
                    </div>
                        
        </div>
        </section>
        -->
        <!-- /about -->
        <section id="parallax2" class="section parallax" data-stellar-background-ratio="0.5">   
            <div class="align-center pad-top40 pad-bot40">
                <blockquote class="bigquote color-white">We’d love to hear about your needs and challenges.</blockquote>
                <!-- <p class="color-white">Bootstraptaste</p> -->
            </div>
        </section>

        <!-- contact -->
        <section id="section-contact" class="section appear clearfix">
            <div class="container">
                
                <div class="row mar-bot40">
                    <div class="col-md-offset-3 col-md-6">
                        <div class="section-header">
                            <h2 class="section-heading animated" data-animation="bounceInUp">Contact us</h2>
                            <p>For more information about our product and service, please send us an email.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="cform" id="contact-form">
                            <div id="sendmessage">
                                 Your message has been sent. Thank you!
                            </div>
                            <form action="contact/contact.php" method="post" role="form" class="contactForm">
                              <div class="form-group">
                                <label for="name">Your Name</label>
                                <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="maxlen:4" data-msg="Please enter at least 4 chars" />
                                <div class="validation"></div>
                              </div>
                              <div class="form-group">
                                <label for="email">Your Email</label>
                                <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                                <div class="validation"></div>
                              </div>
                              <div class="form-group">
                                <label for="subject">Subject</label>
                                <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="maxlen:4" data-msg="Please enter at least 8 chars of subject" />
                                <div class="validation"></div>
                              </div>
                              <div class="form-group">
                                <label for="message">Message</label>
                                <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us"></textarea>
                                <div class="validation"></div>
                              </div>
                              
                              <button type="submit" class="btn btn-theme pull-left">SEND MESSAGE</button>
                            </form>

                        </div>
                    </div>
                    <!-- ./span12 -->
                </div>
                
            </div>
        </section>
        
    <section id="footer" class="section footer">
            <div class="row align-center copyright">
                    <div class="col-sm-12"><p>Copyright &copy; 2015 Wolksen</sp></div>
            </div>
        </div>

    </section>
    <a href="#header" class="scrollup"><i class="fa fa-chevron-up"></i></a> 

    <script src="js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <script src="js/jquery.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/jquery.nicescroll.min.js"></script>
    <script src="js/fancybox/jquery.fancybox.pack.js"></script>
    <script src="js/skrollr.min.js"></script>       
    <script src="js/jquery.scrollTo-1.4.3.1-min.js"></script>
    <script src="js/jquery.localscroll-1.2.7-min.js"></script>
    <script src="js/stellar.js"></script>
    <script src="js/jquery.appear.js"></script>
    <script src="js/validate.js"></script>
    <script src="js/main.js"></script>

    </body>
</html>
